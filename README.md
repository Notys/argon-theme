# Argon-Theme
Argon - 一个轻盈、简洁、美观的 WordPress 主题

[![GitHub release](https://img.shields.io/github/v/release/abc2237512422/argon-theme?color=%235e72e4&style=for-the-badge)](https://github.com/abc2237512422/argon-theme/releases) [![GitHub All Releases](https://img.shields.io/github/downloads/abc2237512422/argon-theme/total?style=for-the-badge)](https://github.com/abc2237512422/argon-theme/releases) [![GitHub](https://img.shields.io/github/license/abc2237512422/argon-theme?color=blue&style=for-the-badge)](https://github.com/abc2237512422/argon-theme/blob/master/LICENSE) [![Author]( https://img.shields.io/badge/author-abc2237512422-yellow?style=for-the-badge)](https://github.com/abc2237512422) [![GitHub stars](https://img.shields.io/github/stars/abc2237512422/argon-theme?color=ff69b4&style=for-the-badge)](https://github.com/abc2237512422/argon-theme/stargazers)

[![GitHub last commit](https://img.shields.io/github/last-commit/abc2237512422/argon-theme?style=flat-square)](https://github.com/abc2237512422/argon-theme/commits/master) [![GitHub Release Date](https://img.shields.io/github/release-date/abc2237512422/argon-theme?style=flat-square)](https://github.com/abc2237512422/argon-theme/releases) ![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/abc2237512422/argon-theme?style=flat-square) 

# 特性

- 使用 Argon Design System 前端框架，轻盈美观
- 丰富的自定义选项 （顶栏，侧栏，头图等）
- 顶栏、侧栏完全自定义 （自定义链接，图标，博客名，二级菜单等）
- 丰富的可自定义侧栏内容 （作者名称，格言，作者信息，作者链接，友情链接，分类目录，所有标签等）
- 可设置主题色
- 内置 "说说" 功能，随时发表想法
- 支持在侧栏添加小工具
- 良好的阅读体验
- 侧栏浮动文章目录
- 自动计算字数和阅读时间
- Pjax 无刷新加载
- Ajax 评论
- 内置多种小工具（进度条，TODO 复选框，标签等）
- 内置 Mathjax、平滑滚动等
- 支持自定义 CSS 和 JS
- 适配小屏幕设备
- 夜间模式支持

# 安装

在 [Release](https://github.com/abc2237512422/argon-theme/releases) 页面下载 .zip 文件，在 WordPress 后台 "主题" 页面上传并安装。

# 文档

[Argon-Theme 文档 : https://abc233.site/argon-theme-docs](https://abc233.site/argon-theme-docs)

# Demo

主题效果预览

[abc233.site](https://abc233.site)

# 注意

Argon 使用 [GPL V3.0](https://github.com/abc2237512422/argon-theme/blob/master/LICENSE) 协议开源，请遵守此协议进行二次开发等。

您**必须在页脚保留 Argon 主题的名称及其链接**，否则请不要使用 Argon 主题。

您**可以删除**页脚的作者信息，但是**不能删除** Argon 主题的名称和链接。

# 截图

![截图1](https://abc233.site/argon-theme-docs/img/screenshot1.png)

![截图2](https://abc233.site/argon-theme-docs/img/screenshot2.png)

![截图3](https://abc233.site/argon-theme-docs/img/screenshot3.png)

![截图4](https://abc233.site/argon-theme-docs/img/screenshot4.png)

# 更新日志

## 20200121 v0.610
+ 重构切换主题功能
+ 修复 CSS 的一堆问题
+ 修复 Pjax 带 `target="blank"` 属性的 `a` 标签在本页打开的问题
+ 一些小改进

## 20200116 v0.601
+ 进一步适配主题色 (如滚动条颜色，`a` 标签下划线颜色等)

## 20200116 v0.600
+ 增加博客主题色选项，可自定义主题色
+ 增加 SEO Meta 标签
+ 修复 Pjax 的一个 BUG

## 20200105 v0.597
+ 修复之前没发现的一个无关紧要的小问题

## 20200104
+ 更改协议为 GPL V3.0

## 20191231 v0.596
+ 修复设置界面的小问题

## 20191221 v0.595
+ 平滑滚动增加脉冲式滚动的选项 (Edge 式滚动)

## 20191216 v0.594
+ Argon 后台设置增加浮动目录
+ 增加文章目录显示序号选项
+ 修复左侧栏 Tab 的显示问题
+ 修复左侧栏浮动时在特定屏幕尺寸下的显示问题

## 20191214 v0.593
+ 博客设置增加阴影选项
+ 修复界面的一些问题
+ 修复其他的一些小问题
+ 升级 Argon 框架到 1.1.0 版本

## 20191214 v0.592
+ 加入博客设置功能
	+ 位于浮动操作按钮栏
	+ 设置选项：夜间模式、字体（衬线/无衬线）、页面滤镜
	+ 默认关闭浮动操作按钮栏的夜间模式切换按钮（与设置菜单中重复），可以在 Argon 设置中手动开启
+ 微调 CSS
+ 其他小改动

## 20191204 v0.591
+ 增加进入文章过渡动画选项（测试）

## 20191111 v0.590
+ 增加博客公告功能

## 20191107 v0.582
+ 修复未开启 Mathjax 选项时 Pjax 错误的问题

## 20191104 v0.581
+ 支持切换主题更新源
+ 修复 CSS 一个小问题

## 20191104 v0.58
+ 优化设置页面
+ 修复评论框高度错误问题

## 20191029 v0.57
+ 增加 题图(特色图片) 的支持

## 20191026 v0.56
+ 提升 Mathjax 版本到 3.0
+ 更换默认 Mathjax CDN
+ 允许自定义 Mathjax CDN
+ 修复由于 Mathjax 文件未加载成功导致 Pjax 错误的问题

## 20191023 v0.55
+ 修复手机端侧栏的小问题
+ 提升后台管理中"Argon 主题选项"菜单层级
+ 采用新的检测更新库，修复更新问题
+ 其他细节调整

## 20191017 v0.54
+ 修改手机端侧栏效果
+ 合并 CSS 文件
+ 细节微调
+ 修改加密博客阅读量统计逻辑

## 20191014 v0.53
+ 增加赞赏二维码选项
+ 增加视频短代码
+ 修改 Pjax 逻辑
+ 增加首页文章浏览不显示短代码选项
+ 修复夜间模式的一个小问题

## 20191013 v0.52
+ 增加安装统计
+ 增加时区修复

## 20191012 v0.51
+ "说说"增加点赞功能
+ 微调弹出提示的样式

## 20191010 v0.5
+ 增加 "说说" 功能
+ 增加 Github Repo 信息短代码
+ 细节修改

## 20190923 v0.4
+ 如果某个菜单没配置，会默认隐藏，不再会影响观感
+ 修复了检测更新的一个问题
+ 增加"隐藏文字"短代码，在鼠标移上时才会显示
+ 修复图片放大模糊的问题
+ Banner 支持必应每日一图
+ 适配 Android Chrome Toolbar 颜色
+ 待审核评论会打上标签提示发送者
+ 修复 Pjax 加载后评论框大小不随内容调整的 BUG
+ 夜间模式全屏放大图片图片颜色不会变暗了
+ 修复了 CSS 的一些问题
+ 修复其他一些小问题

## 20190907 v0.31
+ 修复调试时遗留下来的一个 BUG

## 20190904 v0.3
+ Pjax 加载时替换 WordPress Adminbar
+ 修复后台评论提示验证码错误问题
+ 手机减小文章页面 margin
+ Pjax 加载逻辑修改
+ 博主评论免验证码

## 20190829 v0.2
+ 修复一些 BUG
+ checkbox 增加可选的 `inline` 属性
+ 针对 Wordpress 管理条进行处理
+ 修复夜间模式的一些问题
+ 修改一些细节
